import bin.face_c as face
import bin.db_api as db_api
import cv2
import eigenspace.eigenlib as e
db = db_api.db_manager()
import numpy as np
import json

#db.initialize()
#db.fill("feedBdd/dataset.json") # ok
#cv2.imshow('test',db.avFace())  # ok
#cv2.waitKey(0)
#v = db.makeEigenspace()
eigenspace = json.load(open("eigenspace.json"))
db.train()