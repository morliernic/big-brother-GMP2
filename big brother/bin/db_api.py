import sqlite3
import json
from . import face_c
import cv2 as cv
from eigenspace import eigenlib
import numpy as np


class db_manager:

    def __init__(self):
        self.conn = sqlite3.connect("bin/bdd/bigBro.db")

    def initialize(self):
        c = self.conn.cursor()
        c.execute("drop table image_data")
        c.execute("drop table facebook")
        c.execute("CREATE TABLE facebook (id INTEGER PRIMARY KEY DEFAULT 1,nom varchar(30),prenom varchar(30), int projection);")
        c.execute("CREATE TABLE image_data (id INTEGER PRIMARY KEY DEFAULT 1,face_id int not null,path varchar(255) not null,eigenweight varchar(255), CONSTRAINT fk_face_id FOREIGN KEY (face_id) REFERENCES facebook(id));")

    def fill(self, jsonfile):
        data = json.load(open(jsonfile, 'r'))
        for f in data["face"]:
            nom = f["nom"]
            prenom = f["prenom"]
            taille = int(f["taille"])
            for p in f["path"]:
                face_c.Face(taille, p, prenom, nom).upload(self.conn)

    def avFace(self):
        c = self.conn.cursor()
        faces = []
        paths = c.execute('SELECT path from image_data').fetchall()
        for p in paths:
            faces.append(np.uint16(cv.imread(p[0], cv.COLOR_BGR2GRAY)))
        averageFace = np.uint8(sum(faces) / len(faces))
        return averageFace

    def query(self, q):
        c = self.conn.cursor()
        return c.execute(q).fetchall()

    def makeEigenspace(self):
        c = self.conn.cursor()
        faces = c.execute("SELECT path from image_data").fetchall()
        print(faces)
        vectors = []
        av = self.avFace().flatten()
        for p in faces:
            i = cv.imread(p[0], cv.COLOR_BGR2GRAY)
            vectors = vectors + [(av - i.flatten()).tolist()]

        #t = np.matmul(np.transpose(np.asarray(vectors)), np.asarray(vectors))
        vectors = np.transpose(np.asarray(vectors))
        space = eigenlib.Matrix(vectors.tolist())
        print("space size : ",space.dimN, " ", space.dimM)
        eigenspace = space.eigenspace(2)
        space.rip()
        eigenspace.writeJson()
        #eigenspace.rip()
        return eigenspace

    def train(self):
        c = self.conn.cursor()
        av = self.avFace().flatten()
        eigenspace = np.asarray(json.load(open("eigenspace.json")))
        faces = c.execute("SELECT face_id, path from image_data").fetchall()
        for p in faces:
            print(p)
            i = cv.imread(p[1],cv.COLOR_BGR2GRAY).flatten()
            s = proj(i, av, eigenspace)
            print(s)
            c.execute("UPDATE image_data SET eigenweight = ? WHERE face_id = ?", (s, p[0]))



def proj(im, av, eigenspace):
    return np.matmul(eigenspace,np.asarray((im - av)))