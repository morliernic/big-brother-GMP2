import numpy as np
import cv2 as cv
import sqlite3
import pathlib
import eigenspace.eigenlib as jdg
from . import db_api


class Face:
    def __init__(self, taille, image, nom=None,prenom=None):
        self.dimention = taille
        self.nom = nom
        self.prenom = prenom
        self.data = process(image, taille)

    def upload(self, conn):
        c = conn.cursor()
        id_face = c.execute('SELECT id FROM facebook WHERE prenom = ? AND nom = ?', (self.prenom, self.nom)).fetchall()
        path = self.prenom + " " + self.nom
        if len(id_face) == 0:
            pathlib.Path('data/'+path).mkdir(exist_ok=True)
            c.execute('INSERT INTO facebook (nom,prenom) VALUES (?,?)', (self.nom, self.prenom))
        id = c.execute("SELECT count(id) FROM image_data").fetchall()[0][0]
        if id is None:
            id=0
        else :
            id = int(id)+1
        path = "data/" + path + "/" + str(id) + ".jpg"
        cv.imwrite(path, self.data)
        id_face = c.execute('SELECT id FROM facebook WHERE prenom = ? AND nom = ?',
                            (self.prenom, self.nom)).fetchall()[0][0]
        c.execute('INSERT INTO image_data (face_id,path) VALUES (?,?)', (id_face, path))
        conn.commit()



def process(image,taille):
    face_cascade = cv.CascadeClassifier('features/haarcascade_frontalface_default.xml')

    print(image)
    img = cv.imread(image)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 3)
    x, y, w, h = faces[0]
    R = max(w,h)
    border = int(R*0.15)
    r_frame = gray[(y-border):(y + R + border),(x - border):(x + border + R)]
    cv.rectangle(img, (x-border, y-border), (x + R + border, y + R + border), (255, 0, 0), 2)
    r_frame = cv.resize(r_frame,None, fx = taille/(R+border*2), fy = taille/(R+border*2))
    return r_frame


