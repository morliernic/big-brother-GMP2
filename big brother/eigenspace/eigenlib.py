import ctypes
import shutil
import numpy as np
import os, sys

#shutil.copy2('C:/Users/Admin/Nextcloud/grugeisti/info S2/eigenspace2/cmake-build-windowscompilerx64/eigenspace.dll', 'eigenspace.dll')
lib = ctypes.CDLL('eigenspace/eigenspace.dll')


class Matrix(object):
    def __init__(self, data=None, obj_p=None):
        lib.Matrix_new.argtypes = [ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_double)]
        lib.Matrix_new.restype = ctypes.c_void_p

        lib.Matrix_values.argtypes = [ctypes.c_void_p]
        lib.Matrix_values.restype = ctypes.c_char_p

        lib.Matrix_makeEigenspace.argtypes = [ctypes.c_void_p, ctypes.c_int]
        lib.Matrix_makeEigenspace.restype = ctypes.c_void_p

        lib.Matrix_eigenVector.argtypes = [ctypes.c_void_p, ctypes.c_int]
        lib.Matrix_eigenVector.restype = ctypes.POINTER(ctypes.c_double)

        lib.Matrix_rip.argtypes = [ctypes.c_void_p]

        lib.Matrix_writeJson.argtypes = [ctypes.c_void_p]

        if data is not None:
            data = np.asarray(data, np.double)
            data_p = data.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
            self.dimN = len(data)
            self.dimM = len(data[0])
            self.obj = lib.Matrix_new(self.dimN, self.dimM, data_p)

        else:
            self.obj = obj_p


    def values(self):
        lib.Matrix_values(self.obj)
        #print(ctypes.string_at(lib.Matrix_values(self.obj)).decode('utf-8'))


    def rip(self):
        lib.Matrix_rip(self.obj)

    def eigenface(self, nb):
        pointer = lib.Matrix_eigenVector(self.obj, nb)
        arr = np.ctypeslib.as_array(pointer,(self.dimN, nb))
        #lib.del_Arr(pointer)
        return arr


    def eigenspace(self, nb):
        #arr = np.ctypeslib.as_array(pointer,(self.dimM, nb))
        #lib.del_Arr(pointer)
        t = Matrix(None, lib.Matrix_makeEigenspace(self.obj, nb))
        t.dimM = nb
        t.dimN = self.dimN
        return t

    def writeJson(self):
        lib.Matrix_writeJson(self.obj)

